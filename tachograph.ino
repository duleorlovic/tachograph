//
// Kayak Electronics www.kayak-electronics.com
// 
// Double accurate contactless Tachograph 
// Januar 2013.

// commands to run on Ubuntu for visualisation of sensor analog readings
// http://www.lysium.de/blog/index.php?/archives/234-Plotting-data-with-gnuplot-in-real-time.html
// screen -L /dev/ttyACM0
// cat /dev/ttyACM0 | tee sensors.log | ./driveGnuPlotStreams.pl 4 2 60 60 0 1100 0 1100 400x600+0+0 400x600+0+0 'sensorValue1' 'state1' 'senorValue2' 'state2' 0 0 1 1

//#define USE_SERIAL
//#define USE_SERIAL_DEBUG

// comi's black&white sensor is counter1
#define SENSOR1 A2
#define SENSOR1_LEVEL 2 //30
//#define SENSOR1_PERIOD 4  we assume this is the fact

// dule's line of sight sensor is counter2
#define SENSOR2 A3
#define SENSOR2_LEVEL_HIGH 800
#define SENSOR2_LEVEL_LOW 400
//#define SENSOR2_PERIOD 4 we assume this is the fact

// buzzer
#define SOUND_PIN 2

// cylinder size
//#define PERIMETER 0.122  // 0.474 / 4    novo 488
#include <EEPROM.h>

#include <LiquidCrystal.h>
LiquidCrystal lcd(8, 9, 4, 5, 6, 7); //lcd(7, 6, 5, 4, 3, 2);
// initialize the library with the numbers of the interface pins

byte perimeter = 0;     // here is perimeter/4 in mm 122, times 4 gives full perimeter
int oneHundredMeter = 0;  // how many counter per 100 m, this number is used for beep
float perimeterInMM = 0;

int counter1 = 0;        // here we store main counter
bool detected1 = true;           // state of a sensor (LOW/HIGH)
long lastDebounceTime1 = 0;  // the last time the output pin was toggled
#define MIN_BLACK_INTERVAL 20    // at least this time has to be 0 (no light)  does not work for 15ms because there is huge low sin period

int counter2 = 0;       // here we store main counter
int detected2 = false;          // state of a sensor (LOW/HIGH)
#define DEBOUNCE_DELAY 10    // after LOW and after HIGH wait some time

#ifdef USE_SERIAL_DEBUG
int lastSensorReading1;  // variable used for sampling value
long lastSensorReadingTime1; // variable used for sampling time
int newState1;
int lastSensorReading2; // variable used for sampling value
long lastSensorReadingTime2;// variable used for sampling time
int newState2;
#endif

int lastInterval[2];         // interval between last two detections
unsigned long lastMillis[2]; // last time it was detected
int missedDetection[2];    // number of missedDetection

long soundTime = 1;        // time when sound turned on

// print value on Lcd with right align
// set printDecimalPlaces to true if you want to print two decimal places
void printLcdRightAlign( float value, int col, int row, int printDecimalPlaces = false);
void printLcdRightAlignIncrement( int value, int col, int row);

void setup()
{
 lcd.begin(16, 2);
#ifdef USE_SERIAL 
 Serial.begin(9600);
#endif 
 pinMode(SOUND_PIN, OUTPUT);
 perimeter = EEPROM.read(0);
 lcd.setCursor( 0, 0 );
 lcd.print( "razmak        mm" );       
 lcd.setCursor( 0, 1 );
 lcd.print( "ceo krug      mm" );
 printLcdRightAlign( perimeter , 12, 0); // value, col, row
 printLcdRightAlign( perimeter * 4 , 12, 1); // value, col, row
 int adc_key_in;
 long now = millis();
 bool isChanged = false;
 while (millis() - now < 2500 || isChanged)
 {
   adc_key_in = analogRead(0);
   if (adc_key_in < 1020)
   {
     // key is pressed
     isChanged = true;
     digitalWrite(SOUND_PIN,HIGH);
     delay(300);
     digitalWrite(SOUND_PIN,LOW);
     if (adc_key_in < (328+143)/2)
     {
       // push up
       perimeter++;
     }
     else
     {
       // push down
       perimeter--;
       lcd.setCursor( 0, 0 );
       lcd.print( "razmak        mm" );       
       lcd.setCursor( 0, 1 );
       lcd.print( "ceo krug      mm" );     
     }
     printLcdRightAlign( perimeter , 12, 0); // value, col, row
     printLcdRightAlign( perimeter * 4 , 12, 1); // value, col, row 
     EEPROM.write(0, perimeter);
   } // if (adc_key_in < 1020)
 }
 lcd.setCursor( 0, 0 );
 lcd.print( "                " );       
 lcd.setCursor( 0, 1 );
 lcd.print( "                " );       
 printLcdRightAlign( counter1, 6, 0); // value, col, row
 printLcdRightAlign( counter1 * perimeter , 15, 0, true ); // value, col, row, printDecimalPlaces
 printLcdRightAlign( counter2, 6, 1); // value, col, row
 printLcdRightAlign( counter2 * perimeter , 15, 1, true);
 digitalWrite(SOUND_PIN,HIGH);
 delay(500);
 digitalWrite(SOUND_PIN,LOW);
 
 oneHundredMeter = 1000/((float)perimeter/100);  // for example 819 is 100m = 100/0.122 = 100*1000/122  , 100.000 is int overflow, so do division first
#ifdef USE_SERIAL
  Serial.print( "oneHundredMeter:");
  Serial.println( oneHundredMeter);
#endif
  perimeterInMM = (float)perimeter/1000;
}

void loop(){
  readSensor1();
  readSensor2();
  if (soundTime == 0 && counter2 % oneHundredMeter == 0)
  {
    digitalWrite(SOUND_PIN,HIGH);
    soundTime = millis();   
  }
  else
  {
    if (millis() - soundTime > 3000)
    {
       digitalWrite(SOUND_PIN,LOW);
       if (counter2 % oneHundredMeter != 0)
       {
          soundTime = 0;
       }
    }
  }
}

// comi's black & white sensor
void readSensor1() 
{
  int sensorReading = analogRead(SENSOR1);  
#ifdef USE_SERIAL_DEBUG
  // sampling every 100ms for big difference of sensorReading and lastSensorReading
  // sampling every 500ms for small fluctuation or steady state
  if (((abs(sensorReading - lastSensorReading1)>10) && (millis()-lastSensorReadingTime1>100)) ||
          (millis() - lastSensorReadingTime1> 500)) {
    lastSensorReading1 = sensorReading ;
    lastSensorReadingTime1 = millis();
    // 0: is sensorReading
    Serial.print( "0:");
    Serial.println( sensorReading);
    // 1: is actual state show on graph as high or low
    if (newState1 == LOW)
      Serial.println( "1:20");
    else
      Serial.println( "1:1000");
  }
#endif  
  if (sensorReading > SENSOR1_LEVEL)
  {
    lastDebounceTime1 = millis();
    if (detected1 == false)
    {
      detected1 = true;
      counter1++;
      printLcdRightAlignIncrement( counter1 , 6, 0);
      printLcdRightAlign( counter1 * perimeterInMM , 15, 0, true);
  //   checkDiff( SENSOR1 - SENSOR1 ); 
    }
  }
  else
  {
    if (detected1 && millis()-lastDebounceTime1 > MIN_BLACK_INTERVAL)
    {    
     detected1 = false;
    }
  }

   
}

void readSensor2() 
{
#ifdef USE_SERIAL_DEBUG  
  int sensorReading = analogRead(SENSOR2);  
  // sampling every 100ms for big difference
  // sampling every 500ms for small fluctuation or steady state
  if (((abs(sensorReading - lastSensorReading2)>10)  && (millis()-lastSensorReadingTime1>100)) ||
      (millis() - lastSensorReadingTime2> 500)) {
    lastSensorReading2 = sensorReading ;
    lastSensorReadingTime2 = millis();
    // 2: is sensorReading
    Serial.print( "2:");
    Serial.println( sensorReading);
    // 3: is actual state show on graph as high or low
    if (newState2 == LOW)
      Serial.println( "3:230");
    else
      Serial.println( "3:1030");
  }
#endif
  if (!detected2 && analogRead(SENSOR2) < SENSOR2_LEVEL_LOW)
  {
    detected2 = true;
    delay(10);
  }
  if ( detected2 && analogRead(SENSOR2) > SENSOR2_LEVEL_HIGH)
  {
    detected2 = false;
    counter2++;        
    printLcdRightAlignIncrement( counter2, 6, 1);
    printLcdRightAlign( counter2 * perimeterInMM , 15, 1, true);
    //    checkDiff( SENSOR2  - SENSOR1);
    delay(5);
  }
}
void printLcdRightAlignIncrement( int data, int col, int row )
{
  if (data%10 != 0)
  {
    lcd.setCursor( col, row );
    lcd.print( data%10 );
    return;
  }
  lcd.setCursor( col, row );
  lcd.print( 0 );
  col--;
  if ((data/10)%10 != 0)
  {    
    lcd.setCursor( col, row );
    lcd.print( (data/10)%10  );
    return;
  }
  lcd.setCursor( col, row );
  lcd.print( 0 );
  col--;
  if ((data/100)%10 != 0)
  {
    lcd.setCursor( col, row );
    lcd.print( (data/100)%10  );
    return;
  }
  lcd.setCursor( col, row );
  lcd.print( 0 );
  col--;
  lcd.setCursor( col, row );
  lcd.print( (data/1000)%10 );     
}
void printLcdRightAlign( float value, int col, int row, int printDecimalPlaces )
{
  col -= (int) log10 (value);
  if ( printDecimalPlaces == true )
  {    
    col -= 3;
    lcd.setCursor( col, row );
    lcd.print( value );
  }
  else 
  {
    // value is float so we are typecasting to int
    lcd.setCursor( col, row );
    lcd.print( (long) value);
  }
}

void checkDiff(int sensorID)
{  
  // if there is any difference
  //if ( (abs( (float)counter1 /* / 4 SENSOR1_PERIOD*/ - (float)counter2 /* / 4 SENSOR2_PERIOD*/) > (float)1/4) )
  if (abs(counter1 - counter2) > 2)
  {
    if (((millis() - lastMillis[sensorID]) > 1.5 * lastInterval[sensorID] ) || 
         ( (millis() - lastMillis[sensorID]) < 0.5 * lastInterval[sensorID] ))
    {
      // sensorID is causing that difference
      lcd.setCursor( 0, sensorID ); //col,row
      lcd.print( ++missedDetection[sensorID]);
    }
    else
    {
      if (((millis() - lastMillis[!sensorID]) > 1.5 * lastInterval[!sensorID] ) || 
         ( (millis() - lastMillis[!sensorID]) < 0.5 * lastInterval[!sensorID] ))
      {
        // another sensor is causing that difference
        lcd.setCursor( 0, !sensorID ); //col,row
        lcd.print( ++missedDetection[!sensorID]);      
      }
      else
      {
        // unknown source
        lcd.setCursor( 7, sensorID ); //col,row
        lcd.print( "*" );              
      }
    }
  }
  else 
  {
      lcd.setCursor( 0, 0 ); //col,row
      lcd.print( "  ");          
      lcd.setCursor( 7, 0 ); //col,row
      lcd.print( "  ");              
      lcd.setCursor( 0, 1 ); //col,row
      lcd.print( "  ");
      lcd.setCursor( 7, 1 ); //col,row
      lcd.print( "  ");              
      missedDetection[0] = 0;      
      missedDetection[1] = 0;
  }
  lastInterval[sensorID] = millis() - lastMillis[sensorID];
  lastMillis[sensorID] = millis();
}
